import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './core/containers/home/home.component';

const routes: Routes = [
	 { path: '', component: HomeComponent },
   { path: 'film', loadChildren: () => import('./films/film.module').then(m => m.FilmModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
