import { Component, OnInit } from '@angular/core';
import { People } from '../../models/people.model';
import { PeopleService } from '../../services/people.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FilmService } from 'src/app/films/services/film.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  //#region VARIABLES

  listPeopleComplete: People[];
  listPeopleAux: People[];
  listGender:string[];
  
  formFilter: FormGroup;

  filmTile:string;

  pageCurrent: number = 1;

  //#endregion

  //#region CONSTRUCTORS

  constructor(private peopleService: PeopleService,
              public formBuilder: FormBuilder,
              private filmService: FilmService,) {

      this.formFilter = this.formBuilder.group({
        name: null,
        gender: null
      });
  }

  ngOnInit() {
    this.getListPeople();
  }

  //#endregion

  //#region METHODS

  getListPeople(){
    this.peopleService.getPeople().subscribe(peopleResult => {
      this.listPeopleComplete = peopleResult;
      this.listPeopleAux = peopleResult;
      this.listGender =  this.listPeopleComplete.map((item, index) => {
        return item.gender;
      });
      if(this.listGender.length > 0){
        this.listGender = this.listGender.filter((item, index) => {
          return  this.listGender.indexOf(item) === index;
        });
      }
    });
  }
  
  filterListPeople(){
    this.listPeopleAux = this.listPeopleComplete.filter(people => {

      let name: string = this.formFilter.get("name").value ? this.formFilter.get("name").value.toString().toUpperCase() : null;
      let gender: string = this.formFilter.get("gender").value;

      if(name && people.name.toUpperCase().includes(name) == false){
        return false;
      }

      if(gender && people.gender != gender){
        return false;
      }
      return true;

    });

  }

  pageChanged(pageNumber: number){
    this.pageCurrent = pageNumber;
  }

  //#endregion

  //#region EVENTS

  onClickSearch(){
    this.filterListPeople();
  }

  onClickClearSearch(){
    this.formFilter = this.formBuilder.group({
      name: null,
      gender: null
    });
    this.onClickSearch();
  }

  onMouseEnterFilmName(id){
    this.filmService.getFilm(id).subscribe(filmResult => {
      this.filmTile = filmResult.title;
    });
  }

  //#endregion

}
