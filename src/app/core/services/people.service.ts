import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { People, PeopleResult } from "../models/people.model";

@Injectable({
    providedIn: 'root'
})
export class PeopleService {
    
    constructor(private http: HttpClient){}

    getPeople(): Observable<People[]>{
        return this.http.get < PeopleResult > ("assets/api/people.json").pipe(map(x => x.results.sort((a: People, b: People) => {
           
            let returnOrder = (a.films.length > b.films.length ? -1 : 1);
        
            if (a.films.length === b.films.length) {
                returnOrder = (a.name > b.name ? 1 : -1);
            }
        
            return returnOrder;
        })));
        }

}