import { Inject, NgModule, Optional, SkipSelf } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { HomeComponent } from './containers/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import {NgxPaginationModule} from 'ngx-pagination'; 

@NgModule({
    imports: [
        BrowserModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule
    ],
    exports:[
        HeaderComponent,
        FooterComponent
    ],
    declarations: [
        HomeComponent,
        HeaderComponent,
        FooterComponent
    ]
})

export class CoreModule{
    constructor(@Optional() @SkipSelf() @Inject(CoreModule) parentModule: CoreModule) {
		if (parentModule) {
			throw new Error(
				'CoreModule já foi carregado. Importe ele apenas no AppModule!');
		}
	}
}