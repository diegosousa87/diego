import { Component, OnInit } from '@angular/core';
import { Film } from '../../models/film.model';
import { FilmService } from '../../services/film.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  //#region VARIABLES

  detailsFilm: Film;

  idFilm: number;

  //#endregion

  //#region CONSTRUCTORS

  constructor(
    private filmService: FilmService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.idFilm = parseInt(params['id']);
      this.getFilm();
    });
  }

  //#endregion

  //#region METHODS

  getFilm(){
    this.filmService.getFilm(this.idFilm).subscribe(filmResult => {
      this.detailsFilm = filmResult;
    });
  }

  //#endregion

}
