import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DetailsComponent } from "./containers/details/details.component";
import { FilmRoutingModule } from "./films.routing.module";

@NgModule({
    imports: [
        FilmRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        DetailsComponent
    ]
})

export class FilmModule{}

