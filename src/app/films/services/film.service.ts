import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Film, FilmResult } from "../models/film.model";
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class FilmService {
    
    constructor(private http: HttpClient){}

    getFilm(idFilm: number): Observable<Film>{
        return this.http.get<FilmResult>("assets/api/films.json").pipe(map(x => x.results.find(x => x.id === idFilm)));
    } 

}